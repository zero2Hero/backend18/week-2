package hu.zth.comp.addrbook.rest;

import hu.zth.comp.addrbook.service.BaseDao;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@ApplicationScoped
@Path("/")
public class RestEndpoint {
	
	private static final String NL = "\n";
	
	private BaseDao dao;

	@GET
	@Produces("text/plain")
	public Response doGet() {
		return Response.ok("ZeroToHero verseny 2018 - backend - feladat 2").build();
	}

	@GET
	@Path("/list")
	@Produces("text/plain")
	public Response listContacts() {
		String result = dao.getAll(Contact.class).stream().map(this::contactToString).collect(Collectors.joining());
		return Response.ok(result).build();
	}
	
	private String contactToString(Contact contact) {
		StringBuilder sb = new StringBuilder();
		if (contact instanceof Person) {
			Person p = (Person) contact;
			sb.append("Person: ").append(p.getFirstName()).append(" ").append(p.getLastName());
		} else if (contact instanceof Company) {
			Company c = (Company) contact;
			sb.append("Company: ").append(c.getCompanyName());
		} else {
			sb.append("Unkown contact with id: ").append(contact.getId());
		}
		
		return sb.append(NL).
				append(emailsToString(contact.getEmails())).
				append(addressesToString(contact.getAddresses())).
				append(NL).toString();
	}
	
	private String emailsToString(Set<Email> emails) {
		StringBuilder sb = new StringBuilder();
		if (!emails.isEmpty()) {
			sb.append("  Emails:").append(NL);
			emails.forEach(email -> sb.append("  - ").
					append(email.getType().name()).append(" - ").
					append(email.getEmail()).append(NL));
		}
		return sb.toString();
	}

	private String addressesToString(Set<Address> addresses) {
		StringBuilder sb = new StringBuilder();
		if (!addresses.isEmpty()) {
			sb.append("  Addresses:").append(NL);
			addresses.forEach(addr -> sb.append("  - ").append(addr.getType().name()).append(NL).
					append("    ").append("   Country: ").append(addr.getCountry()).append(NL).
					append("    ").append("      City: ").append(addr.getCity()).append(NL).
					append("    ").append("PostalCode: ").append(addr.getPostalCode()).append(NL).
					append("    ").append("   Address: ").append(addr.getAddress()).append(NL));
		}
		return sb.toString();
	}
	
	@GET
	@Path("/new/{firstName}/{lastName}")
	@Produces("text/plain")
	public Response createPerson(@NotNull @PathParam("firstName") String firstName, @NotNull @PathParam("lastName") String lastName) {
		Person p = new Person();
		p.setFirstName(firstName);
		p.setLastName(lastName);
		dao.createNew(p);
		return Response.ok(String.format("Person created with name: %s %s", firstName, lastName)).build();
	}

	@GET
	@Path("/list/{city}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByCity(@PathParam("city") String city) {
		/* **********************************************************
		 * 
		 * Kérdezd le az adatbázisból Criteria Query -vel azokat
		 * a Contact-okat, melyek rendelkeznek olyan Address-szel,
		 * melynek City értéke megegyezik a metódus
		 * paraméterében kapott értékkel.
		 * 
		 * A kapott listát JSON stringként küldjük a válaszban
		 * 
		 * *********************************************************/
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}
	
}