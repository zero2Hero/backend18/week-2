insert into contact (id, contact_type) values (1, 'C');
insert into contact (id, contact_type) values (2, 'C');
insert into contact (id, contact_type) values (3, 'P');
insert into contact (id, contact_type) values (4, 'P');
insert into contact (id, contact_type) values (5, 'P');
                                             
insert into company (id, companyname) values (1, 'TheCompany Ltd.');
insert into company (id, companyname) values (2, 'Cég Kft.');
                                             
insert into person (id, firstname, lastname) values (3, 'John', 'Doe');
insert into person (id, firstname, lastname) values (4, 'Anon', 'Ymus');
insert into person (id, firstname, lastname) values (5, 'Jane', 'Done');
                                                         
insert into address (id, address, city, country, postalcode, type) values (1, 'Szent Benedek utca 2.', 'Budapest', 'Hungary', '1095', 'OFFICE');
insert into address (id, address, city, country, postalcode, type) values (2, 'Where the street have no name', 'London', 'Great Britain', '12345', 'HOME');
insert into address (id, address, city, country, postalcode, type) values (3, 'Bartók Béla út 4-5. B', 'Budapest', 'Hungary', '1111', 'HOME');
insert into address (id, address, city, country, postalcode, type) values (4, 'Lehel út 86.', 'Budapest', 'Hungary', '1136', 'HEADQUARTER');
                                                                          
insert into contact_address (contact_id, address_id) values (1, 4);       
insert into contact_address (contact_id, address_id) values (2, 1);       
insert into contact_address (contact_id, address_id) values (3, 2);       
insert into contact_address (contact_id, address_id) values (3, 3);       
insert into contact_address (contact_id, address_id) values (5, 4);       
                                                                          
insert into email (id, email, type, contact_id) values (6, 'info@best-company.io', 'WORK', 1);
insert into email (id, email, type, contact_id) values (5, 'support@best-company.io', 'WORK', 1);
                                                         
insert into email (id, email, type, contact_id) values (2, 'person@own-domain.hu', 'HOME', 3);
insert into email (id, email, type, contact_id) values (3, 'employee@company.hu', 'WORK', 3);
insert into email (id, email, type, contact_id) values (4, 'noreply@example.com', 'HOME', 4);
insert into email (id, email, type, contact_id) values (1, 'some-one@gmail.com', 'HOME', 5);
