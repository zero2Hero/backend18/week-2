create table address (id bigint auto_increment primary key, address varchar(200) not null, city varchar(30) not null, country varchar(30) not null, postalcode varchar(10) not null, type varchar not null);
create table contact (id bigint auto_increment primary key, contact_type varchar(31));
create table company (id bigint not null primary key, companyname varchar(100) not null, constraint fk_company_id foreign key (id) references contact);
create table contact_address (contact_id bigint not null, address_id bigint not null, primary key (contact_id, address_id), constraint fk_contact_address_address_id foreign key (address_id) references address, constraint fk_contact_address_contact_id foreign key (contact_id) references contact);
create table email (id bigint auto_increment primary key, email varchar(100) not null, type varchar, contact_id bigint, constraint fk_email_contact_id foreign key (contact_id) references contact);
create table person (id bigint not null primary key, firstname varchar(30) not null, lastname varchar(30) not null, constraint fk_person_id foreign key (id) references contact);
